insert into sales (id, percent) values
(1, 0.2),
(2, 0.5),
(3, 0.8);
insert into products (id, category, name, price, sale_id) values
(1, 'FOOD', 'bread', 10.0, 1),
(2, 'FOOD', 'cheese', 18.0, 2),
(3, 'FOOD', 'butter', 10.0, null),
(4, 'FOOD', 'milk', 7.0, null),
(5, 'FOOD', 'potato', 4.0, 3);
insert into accounts (email, type, name, password) values
('test@gmail.com', 'CLIENT', 'Test', 'password'),
('test2@gmail.com', 'CLIENT', 'Test', 'password');
insert into buckets (id) values (1), (2);
insert into product_in_bucket (id, quantity, product_id) values
(1, 5, 1),
(2, 8, 2),
(3, 3, 3),
(4, 4, 4),
(5, 10, 5),
(6, 10, 5);
insert into bucket_product (bucket_id, product_in_bucket_id) values
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 6);

insert into clients (account_email, balance, bucket_id) values
('test@gmail.com', 178.0, 1),
('test2@gmail.com', 0.0, 2);