package com.za.test.web.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ClientControllerIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    private String registrationJson;
    private String balanceJson;

    @BeforeEach
    void setUp() {
        registrationJson = "{\"name\":\"Test\"," +
                "\"email\":\"test@gmail.com\"," +
                "\"password\":\"Password1!\"," +
                "\"repeatPassword\":\"Password1!\"}";
        balanceJson = "{\"name\":\"Test\"," +
                "\"email\":\"test2@gmail.com\"," +
                "\"money\":\"1000\"}";
    }

    @Test
    @DisplayName("When add new client - should return HTTP status '200' and dto")
    @Transactional
    @Rollback
    void shouldReturnOk() throws Exception {
        mockMvc.perform(post("/create_user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registrationJson))
                .andExpect(ResultMatcher.matchAll(
                        status().isOk(),
                        jsonPath("$.email", is("test@gmail.com")),
                        jsonPath("$.name", is("Test"))));

    }

    @Test
    @DisplayName("When add new client with already exist email- should return HTTP status '404'")
    @Sql(statements = "insert into accounts (email, type, name, password) " +
                    "values ('test@gmail.com', 'CLIENT', 'Test', 'password')")
    @Transactional
    @Rollback
    void shouldReturnBadRequest() throws Exception {
        mockMvc.perform(post("/create_user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registrationJson))
                .andExpect(ResultMatcher.matchAll(
                        status().isBadRequest()));
    }

    @Test
    @DisplayName("When add new client with invalid data- should return HTTP status '400'")
    void shouldReturnBadRequestIfInvalidData() throws Exception {
        //given
        registrationJson = "{\"name\":\"I\"," +
                "\"email\":\"test\"," +
                "\"password\":\"pass\"}";
        //when
        mockMvc.perform(post("/create_user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registrationJson))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isBadRequest(),
                        content().string(containsString("email")),
                        content().string(containsString("name")),
                        content().string(containsString("password")),
                        content().string(containsString("repeatPassword"))
                        ));
    }

    @Test
    @DisplayName("When add balance - should return '202' and dto with updated balance")
    @SqlGroup({
            @Sql(statements = "insert into accounts (email, type, name, password) " +
                    "values ('test2@gmail.com', 'CLIENT', 'Test', 'password')"),
            @Sql(statements = "insert into buckets (id) " +
                    "values (1)"),
            @Sql(statements = "insert into clients (account_email, balance, bucket_id) " +
                    "values ('test2@gmail.com', 0.0, 1)")

    })
    void shouldAddBalance() throws Exception{
        //given
        balanceJson = "{\"name\":\"Test\"," +
                "\"email\":\"test2@gmail.com\"," +
                "\"money\":\"1000\"}";
        //then
        mockMvc.perform(put("/add_money")
                .contentType(MediaType.APPLICATION_JSON)
                .content(balanceJson))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isAccepted(),
                        jsonPath("$.email", is("test2@gmail.com")),
                        jsonPath("$.balance", is(1000.0))
                ));
    }

    @Test
    @DisplayName("When add balance to non-exist client - should return '404'")
    void shouldThrowEntityNotFound() throws Exception{
        //given
        balanceJson = "{\"name\":\"Test\"," +
                "\"email\":\"test3@gmail.com\"," +
                "\"money\":\"1000\"}";
        //then
        mockMvc.perform(put("/add_money")
                .contentType(MediaType.APPLICATION_JSON)
                .content(balanceJson))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isNotFound()
                ));
    }
}
