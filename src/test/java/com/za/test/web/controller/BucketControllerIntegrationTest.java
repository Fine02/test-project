package com.za.test.web.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Sql(scripts = "classpath:/test-data.sql")
@Transactional
public class BucketControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("When pay for bucket - then return '200' and updated dto")
    void shouldPayForBucket() throws Exception {
        mockMvc.perform(put("/pay")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\":\"test@gmail.com\"}"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isOk(),
                        jsonPath("$.balance", is(0.0))
                ));
    }

    @Test
    @DisplayName("When add product to bucket - then return '201' and updated dto")
    void shouldAddProductToBucket() throws Exception {
        mockMvc.perform(post("/pay/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\":\"test2@gmail.com\"}"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isAccepted(),
                        jsonPath("$.products.size()", is(2))
                ));
    }

    @Test
    @DisplayName("When get total price of bucket - then return '200' and double")
    void shouldReturnTotalPrice() throws Exception {
        mockMvc.perform(get("/pay")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\":\"test@gmail.com\"}"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isOk(),
                        content().string(containsString("178.0"))
                ));
    }

    @Test
    @DisplayName("When pay for bucket and not enough balance - then return '403'")
    void shouldThrowPayException() throws Exception {
        mockMvc.perform(put("/pay")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\":\"test2@gmail.com\"}"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isForbidden()
                ));
    }
}
