package com.za.test.web.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProductControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("When get list of products - then return page")
    @SqlGroup({
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (1, 'FOOD', 'cheese', 6.0)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (2, 'TECHNIC', 'phone', 129.0)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (3, 'FOOD', 'bread', 4.7)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (4, 'FOOD', 'tomato', 8.03)" )
    })
    @Transactional
    @Rollback
    void shouldReturnPage() throws Exception{
        mockMvc.perform(get("/products/list"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isOk(),
                        jsonPath("$.currentPage", is(0)),
                        jsonPath("$.totalProducts", is(4)),
                        jsonPath("$.totalPages", is(1)),
                        jsonPath("$.products.size()", is(4))
                ));
    }


    @Test
    @DisplayName("When get list of products by category - then return page")
    @SqlGroup({
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (1, 'FOOD', 'cheese', 6.0)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (2, 'TECHNIC', 'phone', 129.0)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (3, 'FOOD', 'bread', 4.7)" ),
            @Sql(statements = "insert into products (id, category, name, price) " +
                    "values (4, 'FOOD', 'tomato', 8.03)" )
    })
    @Transactional
    @Rollback
    void shouldReturnPageByCategory() throws Exception{
        mockMvc.perform(get("/products/list?category=food"))
                .andDo(print())
                .andExpect(ResultMatcher.matchAll(
                        status().isOk(),
                        jsonPath("$.currentPage", is(0)),
                        jsonPath("$.totalProducts", is(3)),
                        jsonPath("$.totalPages", is(1)),
                        jsonPath("$.products.size()", is(3))
                ));
    }
}
