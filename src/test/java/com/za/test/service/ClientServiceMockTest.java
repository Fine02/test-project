package com.za.test.service;

import com.za.test.data.dao.ClientDao;
import com.za.test.data.entity.Account;
import com.za.test.data.entity.Client;
import com.za.test.exception.AddMoneyException;
import com.za.test.exception.RegistrationException;
import com.za.test.web.dto.AddBalanceDto;
import com.za.test.web.dto.RegistrationDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class ClientServiceMockTest {

    @MockBean
    private ClientDao dao;

    @Autowired
    private ClientService service;

    @Test
    @DisplayName("When save new client and passwords don't match - then throw RegistrationException")
    void shouldThrowRegistrationException() {
        //given
        final RegistrationDto dto = new RegistrationDto("email", "name", "password", "pass");
        //then
        assertThatThrownBy(() -> service.saveNewClient(dto))
                .isInstanceOf(RegistrationException.class);
    }

    @Test
    @DisplayName("When add money and names don't match - then throw AddMoneyException")
    void shouldThrowAddMoneyException() {
        //given
        final AddBalanceDto dto = new AddBalanceDto("email", "name",200.00);
        when(dao.getByEmail("email"))
                .thenReturn(Client.builder()
                .account(Account.builder()
                        .name("Wrong name").build()).build());
        //then
        assertThatThrownBy(() -> service.addBalance(dto))
                .isInstanceOf(AddMoneyException.class);
    }
}
