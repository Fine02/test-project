package com.za.test.service;

import com.za.test.data.dao.ClientDao;
import com.za.test.data.dao.ProductDao;
import com.za.test.data.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class BucketServiceMockTest {

    @MockBean
    private ClientDao clientDao;

    @MockBean
    private ProductDao productDao;

    @Autowired
    private BucketService service;

    @Test
    void shouldReturnPrice() {
        //given
        var product1 = Product.builder()
                .name("bread")
                .price(10.0)
                .sale(new Sale(1L, 0.2)).build();
        var product2 = Product.builder()
                .name("cheese")
                .price(18.0)
                .sale(new Sale(0.5)).build();
        var product3 = Product.builder()
                .name("butter")
                .price(10.0).build();
        var product4 = Product.builder()
                .name("milk")
                .price(7.00).build();
        var product5 = Product.builder()
                .name("potato")
                .price(4.0)
                .sale(new Sale(0.8)).build();
        var account = Account.builder()
                .name("Test")
                .email("test").build();
        var client = Client.builder()
                .id("test")
                .account(account)
                .balance(100.0)
                .bucket(new Bucket(1L, List.of(
                        ProductInBucket.builder().product(product1).quantity(5).build(),
                        ProductInBucket.builder().product(product2).quantity(8).build(),
                        ProductInBucket.builder().product(product3).quantity(3).build(),
                        ProductInBucket.builder().product(product4).quantity(4).build(),
                        ProductInBucket.builder().product(product5).quantity(10).build()
                ))).build();
        when(clientDao.getByEmail("test")).thenReturn(client);

        System.out.println(service.countTotalPrice("test"));
    }
}
