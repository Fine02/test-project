package com.za.test.service;

import com.za.test.data.dao.ProductDao;
import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductCategory;
import com.za.test.web.dto.ProductDto;
import com.za.test.web.dto.ProductPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class ProductServiceMockTest {

    @MockBean
    private ProductDao dao;

    @Autowired
    private ProductService service;

    private Page<Product> products;
    private ProductPage page;

    @BeforeEach
    void setUp() {
        var product = List.of(Product.builder()
                .id(1L)
                .name("bread")
                .category(ProductCategory.FOOD)
                .price(6.0).build());
        var productDto = List.of(ProductDto.builder()
                .id(1L)
                .name("bread")
                .category("FOOD")
                .price(6.0).build());
        products = new PageImpl<>(product);
        page = ProductPage.builder()
                .currentPage(0)
                .totalPages(1)
                .totalProducts(1)
                .products(productDto).build();
    }

    @Test
    @DisplayName("When get all products - then return page")
    void shouldReturnPageByAll() {
        //given
        when(dao.findAll(PageRequest.of(0, 10, Sort.by("name").descending()))).thenReturn(products);
        //when
        var result = service.getPage(0, "none", "desc");
        //then
        assertEquals(page, result);
    }

    @Test
    @DisplayName("When get products by category - then return page")
    void shouldReturnPageByCategory() {
        //given
        when(dao.findByCategory(ProductCategory.FOOD, PageRequest.of(0, 10, Sort.by("name").ascending()))).thenReturn(products);
        //when
        var result = service.getPage(0, "food", "asc");
        //then
        assertEquals(page, result);
    }
}
