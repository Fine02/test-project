package com.za.test.service;

import com.za.test.web.dto.ProductPage;

public interface ProductService {

    ProductPage getPage(Integer page, String category, String order);
}
