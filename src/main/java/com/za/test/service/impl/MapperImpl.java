package com.za.test.service.impl;


import com.za.test.data.entity.Bucket;
import com.za.test.data.entity.Client;
import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductInBucket;
import com.za.test.service.Mapper;
import com.za.test.web.dto.BucketDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.ProductDto;
import com.za.test.web.dto.ProductInBucketDto;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class MapperImpl implements Mapper {

    @Override
    public ClientDto toClientDto(final Client client) {
        return ClientDto.builder()
                .email(client.getAccount().getEmail())
                .name(client.getAccount().getName())
                .balance(client.getBalance())
                .bucket(toBucketDto(client.getBucket())).build();
    }

    @Override
    public BucketDto toBucketDto(final Bucket bucket) {
        return new BucketDto(bucket.getId(),
                bucket.getProducts().stream()
                        .map(this::toProductDto)
                        .collect(Collectors.toList()));
    }

    @Override
    public ProductDto toProductDto(final Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .category(product.getCategory().name())
                .name(product.getName())
                .price(product.getPrice()).build();
    }

    private ProductInBucketDto toProductDto(final ProductInBucket product) {
        return ProductInBucketDto.builder()
                .productDto(toProductDto(product.getProduct()))
                .quantity(product.getQuantity()).build();
    }
}
