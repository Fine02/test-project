package com.za.test.service.impl;

import com.za.test.data.dao.ClientDao;
import com.za.test.data.entity.Account;
import com.za.test.data.entity.AccountType;
import com.za.test.data.entity.Bucket;
import com.za.test.data.entity.Client;
import com.za.test.exception.AddMoneyException;
import com.za.test.exception.RegistrationException;
import com.za.test.service.ClientService;
import com.za.test.service.Mapper;
import com.za.test.web.dto.AddBalanceDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.RegistrationDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Locale;

@RequiredArgsConstructor
@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    private final ClientDao clientDao;
    private final Mapper mapper;

    @Override
    public ClientDto saveNewClient(final RegistrationDto dto) {
        log.info("Start to add new client with email {}", dto.getEmail());

        if(!dto.getPassword().equals(dto.getRepeatPassword())) {
            throw new RegistrationException("Passwords do not match");
        }
        final var newClient = clientDao.save(createDefaultClient(dto));
        log.info("New client saved, id {}", newClient.getId());

        return mapper.toClientDto(newClient);
    }

    @Override
    @Transactional
    public ClientDto addBalance(final AddBalanceDto dto) {
        log.info("Start to add {} money to client balance {}", dto.getMoney(), dto.getEmail());

        final var client = clientDao.getByEmail(dto.getEmail());

        if(!client.getAccount().getName().equals(dto.getName())){
            log.warn("Try to add money for account with email {}, wrong name {}", dto.getEmail(), dto.getName());
            throw new AddMoneyException(String.format("Client name is not %s", dto.getName()));
        }

        client.setBalance(client.getBalance() + dto.getMoney());

        log.info("Added {} money to client balance {}", dto.getMoney(), dto.getEmail());
        return mapper.toClientDto(client);
    }

    private Client createDefaultClient(final RegistrationDto dto){
        return Client.builder()
                .account(Account.builder()
                        .email(dto.getEmail().toLowerCase(Locale.ROOT))
                        .name(dto.getName())
                        .accountType(AccountType.CLIENT)
                        //todo password encoder
                        .password(dto.getPassword()).build())
                .bucket(new Bucket())
                .balance(0.0).build();
    }

}
