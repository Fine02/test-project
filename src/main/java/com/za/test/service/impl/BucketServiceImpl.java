package com.za.test.service.impl;

import com.za.test.data.dao.ClientDao;
import com.za.test.data.dao.ProductDao;
import com.za.test.data.entity.Bucket;
import com.za.test.data.entity.ProductInBucket;
import com.za.test.data.entity.Sale;
import com.za.test.exception.PayException;
import com.za.test.service.BucketService;
import com.za.test.service.Mapper;
import com.za.test.web.dto.BucketDto;
import com.za.test.web.dto.ClientDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BucketServiceImpl implements BucketService {

    private static final Integer SALE_COUNT = 3;

    private final ClientDao clientDao;
    private final ProductDao productDao;
    private final Mapper mapper;

    @Override
    @Transactional
    public BucketDto addProductToBucket(final Long productId, final Integer quantity, final String clientEmail) {
        log.info("Start to add product with id {} to client bucket {}", productId, clientEmail);

        final var bucket = clientDao.getByEmail(clientEmail.toLowerCase(Locale.ROOT)).getBucket();
        bucket.getProducts().add(ProductInBucket.builder()
                .product(productDao.getById(productId))
                .quantity(quantity)
                .build());

        log.info("Added product with id {} to client bucket {}", productId, clientEmail);

        return mapper.toBucketDto(bucket);
    }

    @Override
    public Double countTotalPrice(final String clientEmail) {
        log.info("Get total price for client bucket {}", clientEmail);

        final var price = price(clientDao.getByEmail(clientEmail.toLowerCase(Locale.ROOT)).getBucket());
        log.info("Total price for client bucket {} is {}", clientEmail, price);

        return price;
    }

    @Override
    @Transactional
    public ClientDto payForBucket(final String clientEmail) {
        log.info("Start to pay for client bucket {}", clientEmail);

        final var client = clientDao.getByEmail(clientEmail.toLowerCase(Locale.ROOT));
        final var totalPrice = price(client.getBucket());

        if(client.getBalance() < totalPrice){
            log.warn("Balance of client {} is lower then price", clientEmail);
            throw new PayException(String.format("Balance of client %s is lower then price", clientEmail));
        }

        //todo create order for payed bucket

        client.setBalance(client.getBalance() - totalPrice);
        client.setBucket(new Bucket());

        log.info("Payment for client bucket {} success ", clientEmail);

        return mapper.toClientDto(client);
    }

    private Double price(final Bucket bucket){
        final var productInBuckets = bucket.getProducts().stream()
                .peek(p -> {
                    p.setFullPrice(p.getQuantity()
                            * p.getProduct().getPrice());
                    p.setSale(p.getFullPrice()
                            * Objects.requireNonNullElse(p.getProduct().getSale(),
                            new Sale(0.0)).getPercent());
                })
                .sorted(Comparator.comparing(ProductInBucket::getSale).reversed())
                .collect(Collectors.toList());
        return productInBuckets.stream().limit(SALE_COUNT)
                .mapToDouble(p -> p.getFullPrice() - p.getSale()).sum() +
                productInBuckets.stream().skip(SALE_COUNT)
                        .mapToDouble(ProductInBucket::getFullPrice).sum();
    }
}
