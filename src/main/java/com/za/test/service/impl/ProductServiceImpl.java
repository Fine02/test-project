package com.za.test.service.impl;

import com.za.test.data.dao.ProductDao;
import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductCategory;
import com.za.test.service.Mapper;
import com.za.test.service.ProductService;
import com.za.test.web.dto.ProductPage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    private static final int PAGE_SIZE = 10;
    private static final String SORT_BY = "name";

    private final ProductDao dao;
    private final Mapper mapper;

    @Override
    public ProductPage getPage(final Integer page, final String category, final String order) {
        if("none".equals(category)){
            log.info("Get page {} of products by default", page);

            return getProductPage(dao.findAll(getPageable(page, order)));
        } else {
            log.info("Get page {} of products by category {}", page, category);

            return getProductPage(dao.findByCategory(ProductCategory.valueOf(category.toUpperCase(Locale.ROOT)),
                    getPageable(page, order)));
        }
    }


    private Pageable getPageable(final Integer page, final String order) {
        return "asc".equals(order)
                ? PageRequest.of(page, PAGE_SIZE, Sort.by(SORT_BY).ascending())
                : PageRequest.of(page, PAGE_SIZE, Sort.by(SORT_BY).descending());
    }

    private ProductPage getProductPage(final Page<Product> products){
        return ProductPage.builder()
                .products(products.getContent().stream()
                        .map(mapper::toProductDto)
                        .collect(Collectors.toList()))
                .currentPage(products.getNumber())
                .totalProducts((int) products.getTotalElements())
                .totalPages(products.getTotalPages())
                .build();
    }
}
