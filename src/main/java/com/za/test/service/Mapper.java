package com.za.test.service;

import com.za.test.data.entity.Bucket;
import com.za.test.data.entity.Client;
import com.za.test.data.entity.Product;
import com.za.test.web.dto.BucketDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.ProductDto;

public interface Mapper {

    ClientDto toClientDto(Client client);

    BucketDto toBucketDto(Bucket bucket);

    ProductDto toProductDto(Product product);
}
