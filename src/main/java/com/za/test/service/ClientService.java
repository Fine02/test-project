package com.za.test.service;

import com.za.test.web.dto.AddBalanceDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.RegistrationDto;

public interface ClientService {

    ClientDto saveNewClient(RegistrationDto dto);

    ClientDto addBalance(AddBalanceDto dto);
}
