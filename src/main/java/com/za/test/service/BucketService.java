package com.za.test.service;

import com.za.test.web.dto.BucketDto;
import com.za.test.web.dto.ClientDto;

public interface BucketService {

    BucketDto addProductToBucket(Long productId, Integer quantity, String clientEmail);

    Double countTotalPrice(String clientEmail);

    ClientDto payForBucket(String clientEmail);
}
