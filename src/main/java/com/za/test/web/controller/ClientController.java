package com.za.test.web.controller;

import com.za.test.service.ClientService;
import com.za.test.web.dto.AddBalanceDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.RegistrationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @PostMapping("/create_user")
    public ResponseEntity<ClientDto> saveClient(@RequestBody @Valid RegistrationDto dto){
        return ResponseEntity
                .ok(clientService.saveNewClient(dto));
    }

    @PutMapping("/add_money")
    public ResponseEntity<ClientDto> addBalance(@RequestBody @Valid AddBalanceDto dto){
        return ResponseEntity
                .accepted()
                .body(clientService.addBalance(dto));
    }
}
