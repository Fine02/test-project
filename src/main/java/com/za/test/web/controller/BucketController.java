package com.za.test.web.controller;

import com.za.test.service.BucketService;
import com.za.test.web.dto.BucketDto;
import com.za.test.web.dto.ClientDto;
import com.za.test.web.dto.EmailDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pay")
@RequiredArgsConstructor
public class BucketController {

    private final BucketService service;

    @PostMapping("/{productId}")
    public ResponseEntity<BucketDto> putProductToBucket(@PathVariable final Long productId,
                                                        @RequestBody @Valid final EmailDto clientEmail,
                                                        @RequestParam(defaultValue = "1") final int quantity) {
        return ResponseEntity.accepted()
                .body(service.addProductToBucket(productId, quantity, clientEmail.getEmail()));
    }

    @GetMapping
    public ResponseEntity<Double> getPrice(@RequestBody @Valid final EmailDto clientEmail){
        return ResponseEntity
                .ok(service.countTotalPrice(clientEmail.getEmail()));
    }

    @PutMapping
    public ResponseEntity<ClientDto> payForBucket(@RequestBody @Valid final EmailDto clientEmail){
        return ResponseEntity
                .ok(service.payForBucket(clientEmail.getEmail()));
    }
}
