package com.za.test.web.controller;

import com.za.test.service.ProductService;
import com.za.test.web.dto.ProductPage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;

    @GetMapping("/list")
    public ResponseEntity<ProductPage> getProducts(@RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "none") String category,
                                                   @RequestParam(defaultValue = "asc") String order){
        var prod = service.getPage(page, category, order);
        return ResponseEntity.ok(prod);
    }

}
