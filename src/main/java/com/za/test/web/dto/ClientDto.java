package com.za.test.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClientDto {

    private String email;
    private String name;
    private Double balance;
    private BucketDto bucket;
}
