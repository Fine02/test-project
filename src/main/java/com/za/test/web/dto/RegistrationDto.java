package com.za.test.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegistrationDto {

    @Email(message = "{dto.email.invalid}")
    private String email;

    @NotBlank(message = "{dto.blank}")
    @Size(min = 4, max = 20, message = "{registration.name.size}")
    private String name;

    @Pattern(regexp = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=\\P{N}*\\p{N})[\\s\\S]{8,}$",
            message = "{registration.password.invalid}")
    private String password;

    @NotBlank(message = "{dto.blank}")
    private String repeatPassword;
}
