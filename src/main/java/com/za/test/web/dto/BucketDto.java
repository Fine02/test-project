package com.za.test.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BucketDto {

    private Long id;
    private List<ProductInBucketDto> products;
}
