package com.za.test.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddBalanceDto {

    @Email(message = "{dto.email.invalid}")
    private String email;

    @NotBlank(message = "{dto.blank}")
    private String name;

    @NotNull(message = "{dto.blank}")
    @Positive(message = "{money.positive}")
    private Double money;
}
