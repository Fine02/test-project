package com.za.test.web.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductInBucketDto {

    private ProductDto productDto;
    private Integer quantity;
}
