package com.za.test.web.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ProductDto {

    private Long id;
    private String name;
    private String category;
    private Double price;
}
