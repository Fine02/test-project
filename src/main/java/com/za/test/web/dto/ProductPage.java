package com.za.test.web.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ProductPage {

    private List<ProductDto> products;
    private Integer currentPage;
    private Integer totalProducts;
    private Integer totalPages;
}
