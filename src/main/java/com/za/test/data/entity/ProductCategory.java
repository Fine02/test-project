package com.za.test.data.entity;

public enum ProductCategory {

    FOOD,
    TECHNIC,
    CLOTHES;

}
