package com.za.test.data.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "product_in_bucket")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductInBucket {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "sequence-in-bucket-id")
    @GenericGenerator(
            name = "sequence-in-bucket-id",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "in_bucket_sequence"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "10"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            })
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false, updatable = false)
    @EqualsAndHashCode.Include
    private Product product;

    @Column(name = "quantity")
    private Integer quantity;

    @Transient
    private Double fullPrice;
    @Transient
    private Double sale;

}
