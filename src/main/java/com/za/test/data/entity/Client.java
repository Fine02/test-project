package com.za.test.data.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "clients",
        uniqueConstraints = @UniqueConstraint(columnNames = "bucket_id"))
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Client {

    @Id
    private String id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_email")
    @MapsId
    @EqualsAndHashCode.Include
    private Account account;

    @Column(name = "balance")
    private Double balance;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bucket_id")
    private Bucket bucket;
}
