package com.za.test.data.dao;

import com.za.test.data.entity.Client;

public interface ClientDao {

    Client save(Client client);

    Client getByEmail(String email);
}
