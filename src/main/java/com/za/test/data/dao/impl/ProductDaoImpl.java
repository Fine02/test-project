package com.za.test.data.dao.impl;

import com.za.test.data.dao.ProductDao;
import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductCategory;
import com.za.test.data.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@RequiredArgsConstructor
@Component
public class ProductDaoImpl implements ProductDao {

    private final ProductRepository repository;

    @Override
    public Page<Product> findAll(final Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Page<Product> findByCategory(final ProductCategory category, final Pageable pageable) {
        return repository.findAllByCategory(category, pageable);
    }

    @Override
    public Product getById(final Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Not found product with id %d", id)));
    }
}
