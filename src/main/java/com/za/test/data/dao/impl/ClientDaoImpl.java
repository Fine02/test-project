package com.za.test.data.dao.impl;

import com.za.test.data.dao.ClientDao;
import com.za.test.data.entity.Client;
import com.za.test.data.repository.AccountRepository;
import com.za.test.data.repository.ClientRepository;
import com.za.test.exception.RegistrationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Locale;

@RequiredArgsConstructor
@Component
@Slf4j
public class ClientDaoImpl implements ClientDao {

    private final ClientRepository repository;
    private final AccountRepository accountRepository;

    @Override
    public Client save(final Client client) {
        if(accountRepository.existsById(client.getAccount().getEmail())){
            log.warn("Try to double creating account with email {}", client.getAccount().getEmail());
            throw new RegistrationException(String.format("%s email is already exist", client.getAccount().getEmail()));
        }
        return repository.save(client);
    }

    @Override
    public Client getByEmail(final String email) {
        return repository.findById(email.toLowerCase(Locale.ROOT))
                .orElseThrow(() -> new EntityNotFoundException(String.format("Not found account with email %s", email)));
    }
}
