package com.za.test.data.dao;

import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductDao {

    Page<Product> findAll(Pageable pageable);

    Page<Product> findByCategory(ProductCategory category, Pageable pageable);

    Product getById(Long id);
}
