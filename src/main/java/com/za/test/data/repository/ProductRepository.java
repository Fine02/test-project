package com.za.test.data.repository;

import com.za.test.data.entity.Product;
import com.za.test.data.entity.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Page<Product> findAll(Pageable pageable);

    Page<Product> findAllByCategory(ProductCategory category, Pageable pageable);
}
