package com.za.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AddMoneyException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public AddMoneyException(final String message) {
        super(message);
    }
}
